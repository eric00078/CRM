package com.example.CRM.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
        /*auth.inMemoryAuthentication()
                .withUser("superuser").password("{noop}123").roles("USER", "OPERATOR", "MANAGER", "SUPERUSER")
                .and()
                .withUser("manager").password("{noop}123").roles("USER", "MANAGER")
                .and()
                .withUser("operator").password("{noop}123").roles("USER", "OPERATOR");*/
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http    .authorizeRequests()
                .antMatchers("/h2/**").permitAll()
                .and().csrf().ignoringAntMatchers("/h2/**")
                .and().headers().frameOptions().sameOrigin();

        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/company/**").hasAnyAuthority("user")
                .antMatchers(HttpMethod.GET, "/client/**").hasAnyAuthority("user")
                .antMatchers(HttpMethod.POST, "/company/**").hasAnyAuthority("operator")
                .antMatchers(HttpMethod.POST, "/client/**").hasAnyAuthority("operator")
                .antMatchers(HttpMethod.PUT, "/company/**").hasAnyAuthority("manager")
                .antMatchers(HttpMethod.PUT, "/client/**").hasAnyAuthority("manager")
                .antMatchers(HttpMethod.DELETE, "/company/**").hasAnyAuthority("manager")
                .antMatchers(HttpMethod.DELETE, "/client/**").hasAnyAuthority("manager")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }

}
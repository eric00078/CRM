package com.example.CRM.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "company_id")
    private long company_id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "created_by")
    private String create_by;

    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id", insertable = false, updatable = false)
    private Company company;

    public Client() {

    }

    public Client(long company_id, String name, String email, String phone, String create_by, Timestamp created_at, String updated_by, Timestamp updated_at) {
        this.company_id = company_id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.create_by = create_by;
        this.created_at = created_at;
        this.updated_by = updated_by;
        this.updated_at = updated_at;
    }

    public long getId() {
        return id;
    }

    public long getCompany_id() {
        return company_id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getCreate_by() {
        return create_by;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", company_id=" + company_id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", create_by='" + create_by + '\'' +
                ", created_at=" + created_at +
                ", updated_by='" + updated_by + '\'' +
                ", updated_at=" + updated_at +
                //", company=" + company.toString() +
                '}';
    }
}

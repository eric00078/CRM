package com.example.CRM.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "created_by")
    private String create_by;

    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @JsonBackReference
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<Client> clients;

    public Company() {

    }

    public Company(String name, String address, String create_by, Timestamp created_at, String updated_by, Timestamp updated_at) {
        this.name = name;
        this.address = address;
        this.create_by = create_by;
        this.created_at = created_at;
        this.updated_by = updated_by;
        this.updated_at = updated_at;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCreate_by() {
        return create_by;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", create_by='" + create_by + '\'' +
                ", created_at=" + created_at +
                ", updated_by='" + updated_by + '\'' +
                ", updated_at=" + updated_at +
                '}';
    }
}

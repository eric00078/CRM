package com.example.CRM.service;

import com.example.CRM.model.Client;
import com.example.CRM.model.Company;
import com.example.CRM.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CompanyService {
    @Autowired
    private CompanyRepository repo;

    public List<Company> listAll() {
        return repo.findAll();
    }

    public void save(Company company) {
        repo.save(company);
    }

    public void update(Company company) {
        repo.save(company);
    }

    public Company get(long id) {
        return repo.findById(id).get();
    }

    public void delete(long id) {
        repo.deleteById(id);
    }
}

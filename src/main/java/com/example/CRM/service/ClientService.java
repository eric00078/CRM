package com.example.CRM.service;

import com.example.CRM.model.Client;
import com.example.CRM.model.Company;
import com.example.CRM.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClientService {

    @Autowired
    private ClientRepository repo;

    public List<Client> listAll() {
        return repo.findAll();
    }

    public void save(Client client) {
        repo.save(client);
    }

    public void update(Client client) {
        repo.save(client);
    }

    public Client get(long id) {
        return repo.findById(id).get();
    }

    public void delete(long id) {
        repo.deleteById(id);
    }

    public Company getCompany(long id) {
        return repo.findById(id).get().getCompany();
    }

    public void saveClients(List<Client> clients) {
        repo.saveAll(clients);
    }
}

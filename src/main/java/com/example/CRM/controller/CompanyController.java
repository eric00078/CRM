package com.example.CRM.controller;

import com.example.CRM.model.Company;
import com.example.CRM.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyService service;

    @GetMapping("/companys")
    public List<Company> getAllCompanys() {
        return service.listAll();
    }

    @PostMapping("/new_company")
    public void createNewCompany(@RequestBody Company company) {
        service.save(company);
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable Long id) {
        return service.get(id);
    }

    @PutMapping("/update/{id}")
    public void updateCompany(@RequestBody Company company, @PathVariable Long id) {
        service.save(company);
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable long id) {
        service.delete(id);
    }

}

package com.example.CRM.controller;

import com.example.CRM.model.Client;
import com.example.CRM.model.Company;
import com.example.CRM.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService service;

    @GetMapping("/clients")
    public List<Client> getAllClients() {
        return service.listAll();
    }

    @PostMapping("/new_client")
    public void createNewClient(@RequestBody Client client) {
        service.save(client);
    }

    @GetMapping("/{id}")
    public Client getClient(@PathVariable Long id) {
        return service.get(id);
    }

    @PutMapping("/update/{id}")
    public void updateClient(@RequestBody Client client, @PathVariable Long id) {
        service.save(client);
    }

    @DeleteMapping("/{id}")
    public void deleteClient(@PathVariable long id) {
        service.delete(id);
    }

    @GetMapping("/company/{id}")
    public Company getClientCompany(@PathVariable Long id) {
        return service.getCompany(id);
    }

    @PostMapping("/new_clients")
    public void createNewClients(@RequestBody List<Client> clients) {
        service.saveClients(clients);
    }
}

package com.example.CRM;

import com.example.CRM.model.Client;
import com.example.CRM.model.Company;
import com.example.CRM.model.Role;
import com.example.CRM.model.User;
import com.example.CRM.repository.ClientRepository;
import com.example.CRM.repository.CompanyRepository;
import com.example.CRM.repository.RoleRepository;
import com.example.CRM.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.sql.Timestamp;

@SpringBootApplication
public class CrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmApplication.class, args);
	}

	/*@Bean
	CommandLineRunner initDatabase(CompanyRepository companyRepository, ClientRepository clientRepository, UserRepository userRepository, RoleRepository roleRepository) {
		return args -> {
			Company company = new Company("testCompany1", "testAddress1", "me",  new Timestamp(System.currentTimeMillis()), "me", new Timestamp(System.currentTimeMillis()));
			companyRepository.save(company);
			Client client = new Client(company.getId(), "testClient1", "testEmail1", "testPhone1", "me",  new Timestamp(System.currentTimeMillis()),"me",  new Timestamp(System.currentTimeMillis()));
			clientRepository.save(client);
			User user1 = new User("superuser", new BCryptPasswordEncoder().encode("test123"));
			userRepository.save(user1);
			Role role1 = new Role(user1.getId(), "superuser");
			roleRepository.save(role1);
			Role role2 = new Role(user1.getId(), "manager");
			roleRepository.save(role2);
			Role role3 = new Role(user1.getId(), "operator");
			roleRepository.save(role3);
			Role role4 = new Role(user1.getId(), "user");
			roleRepository.save(role4);
			User user2 = new User("manager", new BCryptPasswordEncoder().encode("test123"));
			userRepository.save(user2);
			Role role5 = new Role(user2.getId(), "manager");
			roleRepository.save(role5);
			Role role6 = new Role(user2.getId(), "user");
			roleRepository.save(role6);
			User user3 = new User("operator", new BCryptPasswordEncoder().encode("test123"));
			userRepository.save(user3);
			Role role7 = new Role(user3.getId(), "manager");
			roleRepository.save(role7);
			Role role8 = new Role(user3.getId(), "user");
			roleRepository.save(role8);
		};
	}*/

}

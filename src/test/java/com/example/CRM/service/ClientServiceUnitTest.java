package com.example.CRM.service;

import com.example.CRM.model.Client;
import com.example.CRM.model.Company;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceUnitTest {

    @Autowired
    private ClientService clientService;

    @Autowired
    private CompanyService companyService;

    private long client_id;

    @Before
    public void init() {
        Company company = new Company("testCompany", "testAddress", "admin",  new Timestamp(System.currentTimeMillis()), "admin", new Timestamp(System.currentTimeMillis()));
        companyService.save(company);
        Client client = new Client(company.getId(), "testClient", "testEmail", "testPhone", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        clientService.save(client);
        client_id = client.getId();
    }

    @Test
    @DirtiesContext
    public void listAll() throws Exception {
        List<Client> clientList = clientService.listAll();

        Assert.assertEquals(1, clientList.size());
        Client client = clientList.get(0);
        Assert.assertNotNull(client);
        Assert.assertEquals("testClient", client.getName());
        Assert.assertEquals("testEmail", client.getEmail());
        Assert.assertEquals("testPhone", client.getPhone());
        Assert.assertEquals("admin", client.getCreate_by());
        Assert.assertNotNull(client.getCreated_at());
        Assert.assertEquals("admin", client.getUpdated_by());
        Assert.assertNotNull(client.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void save() throws Exception {
        Company company = companyService.listAll().get(0);

        Client client = new Client(company.getId(), "testClient1", "testEmail1", "testPhone1", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        clientService.save(client);
        Client client_from_search = clientService.get(client.getId());

        Assert.assertNotNull(client_from_search);
        Assert.assertEquals(client.getName(), client_from_search.getName());
        Assert.assertEquals(client.getEmail(), client_from_search.getEmail());
        Assert.assertEquals(client.getPhone(), client_from_search.getPhone());
        Assert.assertEquals(client.getUpdated_by(), client_from_search.getCreate_by());
        Assert.assertNotNull(client_from_search.getCreated_at());
        Assert.assertEquals(client.getUpdated_by(), client_from_search.getUpdated_by());
        Assert.assertNotNull(client_from_search.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void update() throws Exception {
        Client client = clientService.listAll().get(0);
        client.setName("testClient123");
        clientService.update(client);

        Client client_updated = clientService.listAll().get(0);
        Assert.assertEquals("testClient123", client_updated.getName());
    }

    @Test
    @DirtiesContext
    public void get() throws Exception {
        Client client = clientService.get(client_id);

        Assert.assertNotNull(client);
        Assert.assertEquals("testClient", client.getName());
        Assert.assertEquals("testEmail", client.getEmail());
        Assert.assertEquals("testPhone", client.getPhone());
        Assert.assertEquals("admin", client.getCreate_by());
        Assert.assertNotNull(client.getCreated_at());
        Assert.assertEquals("admin",client.getUpdated_by());
        Assert.assertNotNull(client.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void delete() throws Exception {
        clientService.delete(client_id);

        Assert.assertEquals(0, clientService.listAll().size());
    }

    @Test
    @DirtiesContext
    public void getCompany() throws Exception {
        Company company = clientService.get(client_id).getCompany();

        Assert.assertNotNull(company);
        Assert.assertEquals("testCompany", company.getName());
        Assert.assertEquals("testAddress", company.getAddress());
        Assert.assertEquals("admin", company.getCreate_by());
        Assert.assertNotNull(company.getCreated_at());
        Assert.assertEquals("admin", company.getUpdated_by());
        Assert.assertNotNull(company.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void saveClients() throws Exception {
        Company company = companyService.listAll().get(0);
        List<Client> clientList = new ArrayList<>();
        Client client1 = new Client(company.getId(), "testClient1", "testEmail1", "testPhone1", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        Client client2 = new Client(company.getId(), "testClient2", "testEmail2", "testPhone2", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        clientList.add(client1);
        clientList.add(client2);
        clientService.saveClients(clientList);

        Assert.assertEquals(3, clientService.listAll().size());
    }


}

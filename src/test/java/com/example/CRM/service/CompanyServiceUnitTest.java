package com.example.CRM.service;

import com.example.CRM.model.Company;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyServiceUnitTest {
    @Autowired
    private CompanyService companyService;

    private long company_id;

    @Before
    public void init() {
        Company company = new Company("testCompany", "testAddress", "admin",  new Timestamp(System.currentTimeMillis()), "admin", new Timestamp(System.currentTimeMillis()));
        companyService.save(company);
        company_id = company.getId();
    }

    @Test
    @DirtiesContext
    public void listAll() throws Exception {
        List<Company> companyList = companyService.listAll();

        Assert.assertEquals(1, companyList.size());
        Company company = companyList.get(0);
        Assert.assertNotNull(company);
        Assert.assertEquals("testCompany", company.getName());
        Assert.assertEquals("testAddress", company.getAddress());
        Assert.assertEquals("admin", company.getCreate_by());
        Assert.assertNotNull(company.getCreated_at());
        Assert.assertEquals("admin", company.getUpdated_by());
        Assert.assertNotNull(company.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void save() throws Exception {
        Company company = new Company("testCompany1", "testAddress1", "admin",  new Timestamp(System.currentTimeMillis()), "admin", new Timestamp(System.currentTimeMillis()));
        companyService.save(company);
        Company company_from_search = companyService.get(company.getId());

        Assert.assertNotNull(company_from_search);
        Assert.assertEquals("testCompany1", company_from_search.getName());
        Assert.assertEquals("testAddress1", company_from_search.getAddress());
        Assert.assertEquals("admin", company_from_search.getCreate_by());
        Assert.assertNotNull(company_from_search.getCreated_at());
        Assert.assertEquals("admin", company_from_search.getUpdated_by());
        Assert.assertNotNull(company_from_search.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void update() throws Exception {
        Company company = companyService.listAll().get(0);
        company.setName("testCompany123");
        companyService.update(company);

        Company company_updated = companyService.listAll().get(0);
        Assert.assertEquals("testCompany123", company_updated.getName());
    }

    @Test
    @DirtiesContext
    public void get() throws Exception {
        Company company = companyService.get(company_id);

        Assert.assertNotNull(company);
        Assert.assertEquals("testCompany", company.getName());
        Assert.assertEquals("testAddress", company.getAddress());
        Assert.assertEquals("admin", company.getCreate_by());
        Assert.assertNotNull(company.getCreated_at());
        Assert.assertEquals("admin", company.getUpdated_by());
        Assert.assertNotNull(company.getUpdated_at());
    }

    @Test
    @DirtiesContext
    public void delete() throws Exception {
        companyService.delete(company_id);

        Assert.assertEquals(0, companyService.listAll().size());
    }
}

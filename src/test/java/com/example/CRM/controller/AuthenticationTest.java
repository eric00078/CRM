package com.example.CRM.controller;


import com.example.CRM.model.Client;
import com.example.CRM.model.Company;
import com.example.CRM.service.ClientService;
import com.example.CRM.service.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ClientService clientService;

    @Autowired
    private CompanyService companyService;

    private long company_id;

    @Before
    public void init() {
        Company company = new Company("testCompany", "testAddress", "admin",  new Timestamp(System.currentTimeMillis()), "admin", new Timestamp(System.currentTimeMillis()));
        companyService.save(company);
        company_id = company.getId();
        Client client = new Client(company.getId(), "testClient", "testEmail", "testPhone", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        clientService.save(client);
    }

    @Test
    @WithMockUser(username="superuser", password="test123", authorities={"user"})
    public void validUserForGetCompany() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .get("/company/{id}", company_id)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(company_id));
    }

    @Test
    @WithMockUser(username="superuser", password="test123", authorities={"invalid_user"})
    public void invalidUserForGetCompany() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .get("/company/{id}", company_id)
                .accept(MediaType.ALL_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="superuser", password="test123", authorities={"operator"})
    public void validUserForPostClient() throws Exception {
        Client client = new Client(company_id, "testClient1", "testEmail1", "testPhone1", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        mvc.perform( MockMvcRequestBuilders
                .post("/client/new_client")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username="superuser", password="test123", authorities={"invalid_user"})
    public void invalidUserForPostClient() throws Exception {
        Client client = new Client(company_id, "testClient1", "testEmail1", "testPhone1", "admin",  new Timestamp(System.currentTimeMillis()),"admin",  new Timestamp(System.currentTimeMillis()));
        mvc.perform( MockMvcRequestBuilders
                .post("/client/new_client")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            System.out.println(jsonContent);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
